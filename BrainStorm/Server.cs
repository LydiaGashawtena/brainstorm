﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Net;
using System.IO;
using System.Threading;

namespace BrainStorm
{
    public class Server : IConnection
    {
        private readonly Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private readonly List<Socket> clientSockets = new List<Socket>();
        private readonly List<string> clients = new List<string>();
        private const int BUFFER_SIZE = 2048;
        private const int PORT = 3355;
        private static readonly byte[] buffer = new byte[BUFFER_SIZE];
        private MainWindow mainWindow;
        private Controller controller;
        private string fileToSave;

        public Server(Controller controller)
        {
            this.controller = controller;

        }

        private void SetupServer()
        {
            Console.WriteLine("Setting up server...");
            serverSocket.Bind(new IPEndPoint(IPAddress.Any, PORT));
            serverSocket.Listen(0);
            serverSocket.BeginAccept(AcceptCallback, null);
            Console.WriteLine("Server setup complete");

        }     

        private void AcceptCallback(IAsyncResult AR)
        {
            Socket socket;

            try
            {
                socket = serverSocket.EndAccept(AR);
            }
            catch (ObjectDisposedException) 
            {
                return;
            }

            clientSockets.Add(socket);
            socket.BeginReceive(buffer, 0, BUFFER_SIZE, SocketFlags.None, ReceiveCallback, socket);
            Console.WriteLine("Client connected, waiting for request...");           
            serverSocket.BeginAccept(AcceptCallback, null);
        }

        private void ReceiveCallback(IAsyncResult AR)
        {
            Socket current = (Socket)AR.AsyncState;
            int received;

            try
            {
                received = current.EndReceive(AR);
            }
            catch (SocketException)
            {
                WriteLine("Client forcefully disconnected");              
                current.Close();
                clientSockets.Remove(current);
                return;
            }

            byte[] recBuf = new byte[received];
            Array.Copy(buffer, recBuf, received);

            byte[] action = new byte[4];
            byte[] main = new byte[received - 4];
            Array.Copy(recBuf, 0, action, 0, 4);
            Array.Copy(recBuf, 4, main, 0, received - 4);
            string actionString = Encoding.ASCII.GetString(action);

            string sendString;
            byte[] sendArr, toSend;
            List<byte> temp = new List<byte>();


            switch (actionString)
            {
                case "DRAW":
                    controller.Draw(main);
                    Send("DRAW", main);
                    break;
                case "NCON":
                    WriteLine("CONNECTED: " + Encoding.ASCII.GetString(main));
                    controller.AddNewMember(Encoding.ASCII.GetString(main), "SERVER");

                    sendString = controller.GetMembersForClient();
                    sendArr = Encoding.ASCII.GetBytes(sendString);
                    Send(current, "MEML", sendArr);
                    WriteLine("InServer sent sendArr, action: " + Encoding.ASCII.GetString(sendArr));               

                    Send("NMEM", main);
                    WriteLine("InServer sent sendArr: " + Encoding.ASCII.GetString(sendArr));
                    break;
                case "GNOT":
                    sendString = controller.GetNotesList();
                    sendArr = Encoding.ASCII.GetBytes(sendString);
                    Send(current, "NOTL", sendArr);
                    WriteLine("InServer sent sendArr: " + Encoding.ASCII.GetString(sendArr));
                    break;
                case "FNAM":
                    fileToSave = Encoding.ASCII.GetString(main);
                    WriteLine("In Server received filename: " + fileToSave);
                    controller.Save(fileToSave);
                    break;
                case "FILE":
                    WriteLine("In server received file: " + Encoding.ASCII.GetString(main));
                    controller.Save(fileToSave, main);
                    //controller.Draw(main);
                    break;
                case "GFLS":
                    List<string> files = controller.GetFiles(null, "SERVER");
                    sendString = String.Join(",", files);
                    sendArr = Encoding.ASCII.GetBytes(sendString);
                    Send(current, actionString, sendArr);
                    WriteLine("In server sent files list: " + sendString);
                    break;
                case "GFIL":
                    sendArr = controller.GetAFileForClient(Encoding.ASCII.GetString(main));
                    Send(current, actionString, sendArr);
                    break;
                case "NNOT":
                    controller.AddNote("SERVER", Encoding.ASCII.GetString(main));
                    break;
                
                default:
                    break;
            }            
            if(actionString == "EXIT")
            {
                controller.setConnected(Encoding.ASCII.GetString(main), false);
                current.Shutdown(SocketShutdown.Both);
                current.Close();
                clientSockets.Remove(current);
                Console.WriteLine("Client disconnected");
            }
            else
            {
                current.BeginReceive(buffer, 0, BUFFER_SIZE, SocketFlags.None, ReceiveCallback, current);
            }
            
        }

        private void SendCallback(IAsyncResult AR)
        {
            Socket current = (Socket)AR.AsyncState;
            try
            {
                current.EndSend(AR);
            }
            catch (SocketException ex)
            {
                WriteLine(ex.Message);                
            }
            catch (ObjectDisposedException ex)
            {
                WriteLine(ex.Message);
            }
        }

        public void Connect()
        {
            SetupServer();
        }

        public void Send(string action, byte[] message)
        {
            WriteLine(action + " being sent");
            byte[] actionArr = Encoding.ASCII.GetBytes(action);
            List<byte> temp = new List<byte>();
            byte[] toSend = new byte[actionArr.Length + message.Length];
            for (int i = 0; i < actionArr.Length; i++)
            {
                temp.Add(actionArr[i]);
            }
            for (int i = 0; i < message.Length; i++)
            {
                temp.Add(message[i]);
            }

            toSend = temp.ToArray();
            foreach (Socket sock in clientSockets)
            {
                sock.BeginSend(toSend, 0, toSend.Length, SocketFlags.None, SendCallback, sock);
            }            
            action = null;
            WriteLine("InServer sent toSend: " + Encoding.ASCII.GetString(toSend));
        }

        public void Send(Socket s, string action, byte[] message)
        {
            WriteLine(action + " being sent");
            byte[] actionArr = Encoding.ASCII.GetBytes(action);
            List<byte> temp = new List<byte>();
            byte[] toSend = new byte[actionArr.Length + message.Length];
            for (int i = 0; i < actionArr.Length; i++)
            {
                temp.Add(actionArr[i]);
            }
            for (int i = 0; i < message.Length; i++)
            {
                temp.Add(message[i]);
            }

            toSend = temp.ToArray();
            
            s.BeginSend(toSend, 0, toSend.Length, SocketFlags.None, SendCallback, s);
            WriteLine("InServer sent toSend: " + Encoding.ASCII.GetString(toSend));
        }

        public void Exit()
        {
            serverSocket.Close();
        }
    }
}
