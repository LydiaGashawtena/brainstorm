﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BrainStorm
{
    /// <summary>
    /// Interaction logic for ChooseFileWindow.xaml
    /// </summary>
    public partial class ChooseFileWindow : Window
    {
        public List<String> files = new List<string>();
        public string selected;
        public DatabaseHandler db = new DatabaseHandler();
        public Controller controller;
        public string _for;

        public ChooseFileWindow(Controller controller, string _for)
        {
            this._for = _for;
            this.controller = controller;
            InitializeComponent();
            
        }

        public void SetList(List<string> files)
        {
            this.files = files;
            this.Dispatcher.Invoke((Action)delegate
            {
                fileList.ItemsSource = files;
            });
            
        }


        private void list_selectFile(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                selected = item.Content.ToString();
                Console.WriteLine("Selected: " + selected);
                this.Close();
            }
        }
        
        

    }
}
