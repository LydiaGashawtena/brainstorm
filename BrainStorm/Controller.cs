﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Ink;
using static System.Console;

namespace BrainStorm
{
    public class Controller
    {
        private MainWindow mainWindow;
        private ChooseFileWindow fileWindow;
        private IConnection connection;
        private DatabaseHandler db = new DatabaseHandler();
        private List<string> connectedListForClient = new List<string>();
        private List<string> membersListForClient = new List<string>();
        private List<string> notesListForClient = new List<string>();

        public Controller(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
        }

        public string GetUser()
        {
            return db.SelectOne("name", "user", "''", "");
        }

        public void Connect(string ip)
        {
            connection = new Client(this, ip);
            connection.Connect();
        }

        public void Connect()
        {
            connection = new Server(this);
            connection.Connect();
        }

        public bool isConnected()
        {
            if (connection == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void Draw(byte[] buffer)
        {
            StrokeCollection collection;
            using (MemoryStream ms = new MemoryStream(buffer))
            {                
                collection = new StrokeCollection(ms);
                ms.Close();
            }
            mainWindow.Draw(collection);
            string textrec = Encoding.ASCII.GetString(buffer);
            WriteLine("In draw: " + textrec);           
        }

        public bool Send(StrokeCollection collection) 
        {
            byte[] data;
            using (MemoryStream ms = new MemoryStream())
            {
                collection.Save(ms);
                data = ms.ToArray();
            }
            if (connection == null)
            {
                return false;
            }
            else
            {
                connection.Send("DRAW", data);
                WriteLine("sent: " + Encoding.ASCII.GetString(data));
            }
            return true;
        }

        public void Send(string action, string message)
        {
            byte[] toSend;
            toSend = Encoding.ASCII.GetBytes(message);
            connection.Send(action, toSend);
        }

        public void Exit()
        {
            mainWindow.SetDisconnectedState();
            connection.Exit();
        }

        public string CheckGroup()
        {
           return db.SelectOne("groupName", "user", "''", "");
        }

        public void CreateNewGroup()
        {
            db.Update("user", "hasGroup", "true", "''", "");
            db.Update("user", "groupName", "group", "''", "");
        }

        public List<string> GetMembersForServer()
        {
            return db.SelectAll("name", "members");     
            
        }

        public string GetMembersForClient()
        {
           
            List<string> names = db.SelectAll("name", "members");
            List<string> connected = db.SelectAll("isConnected", "members");
            string namesString = String.Join(",", names);
            string connectedString = String.Join(",", connected);
            return String.Concat(namesString, ";", connectedString);            
           
        }

        public void setMembers(string listString)
        {
            List<string> both = listString.Split(';').ToList();
            membersListForClient = both[0].Split(',').ToList();
            connectedListForClient = both[1].Split(',').ToList();
            mainWindow.MakeMembersList("CLIENT", membersListForClient);
        }

        public bool isConnected(string username, string _for, int index = 0)
        {
            bool isConnected = false;
            if (_for == "SERVER")
            {
                if (db.SelectOne("isConnected", "members", "name", username) == "true")
                {
                    isConnected = true;
                }
                else
                {
                    isConnected = false;
                }
            }
            else if(_for == "CLIENT")
            {
                if (connectedListForClient[index] == "true")
                {
                    isConnected = true;
                }
            }

            return isConnected;
        }

        public void setConnected(string username, bool to)
        {
            if (mainWindow.hasGroup)
            {
                db.Update("members", "isConnected", to.ToString().ToLower(), "name", username);
            }
            else
            {
                db.AddMember(username);
            }
            
            mainWindow.MakeMembersList("SERVER");
        }

        public void AddNewMember(string username, string _for)
        {
            if (_for == "SERVER")
            {
                if (db.SelectOne("isConnected", "members", "name", username) == "")
                {
                    WriteLine("In AddNewMember: " + username);
                    db.AddMember(username);
                }
                else
                {
                    setConnected(username, true);
                }
                mainWindow.MakeMembersList("SERVER");
                
            }
            else
            {
                int index;
                if (membersListForClient.Contains(username))
                {
                    index = membersListForClient.FindIndex(a => a == username);
                    connectedListForClient[index] = "true";
                }
                else
                {
                    membersListForClient.Add(username);
                    connectedListForClient.Add("true");
                }

                mainWindow.MakeMembersList("CLIENT", membersListForClient);
            }
            
        }

        public void RemoveGroup()
        {            
            db.Delete("members");
            db.Delete("files");
            db.Delete("notes");
        }

        public void Save(string _for, string fileName, byte[] file)
        {
            if (_for == "CLIENT")
            {
                connection.Send("FNAM", Encoding.ASCII.GetBytes(fileName));
                connection.Send("FILE", file);
            }
            else
            {
                db.AddFile(fileName, file);
            }
                       
        }

        public void Save(string fileName) {
            db.AddFile(fileName, null);
        }

        public void Save(string fileName, byte[] file)
        {
            WriteLine("In Save: " + fileName);
            db.Update("files", "file", file, "name", fileName);
        }

        public List<string> GetFiles(ChooseFileWindow fileWindow, string _for)
        {
            if (_for == "CLIENT")
            {
                this.fileWindow = fileWindow;
                connection.Send("GFLS", new byte[0]);
                return null;
            }
            else
            {
                return db.SelectAll("name", "files");
            }
        }

        public void Open(string fileName, string _for)
        {
            if (_for == "CLIENT")
            {
                Send("GFIL", fileName);
            }
            else
            {
                byte[] file = db.GetFile(fileName);
                Draw(file);
            }

        }

        public byte[] GetAFileForClient(string fileName)
        {
            return db.GetFile(fileName);
        }

        public void SetAFileForClient(byte[] file)
        {
            Draw(file);
        }

        public void SetFilesForClient(List<string> files)
        {
            fileWindow.SetList(files);
        }

        public void AddNote(string _for, string note)
        {
            if (_for == "CLIENT")
            {
                connection.Send("NNOT", Encoding.ASCII.GetBytes(note));
            }
            else
            {
                db.AddNote(note);
                SetNotesList(_for, db.SelectAll("note", "notes"));
                connection.Send("NNOT", Encoding.ASCII.GetBytes(note));
            }

        }

        public void SetNotesList(string _for, List<string> notes)
        {
            //WriteLine("set: " + notesListForClient[0]);
            if (_for == "CLIENT")
            {
                this.notesListForClient = notes;
            }
            if(notes == null)
            {
                return;
            }
            mainWindow.MakeNotesList(notes);
            WriteLine("SetList");
        }

        public void UpdateNotesList(string note)
        {
            notesListForClient.Add(note);
            SetNotesList("CLIENT" ,notesListForClient);
        }

        public string GetNotesList()
        {
            string listString = String.Join(",", db.SelectAll("note", "notes"));
            return listString;
        }

        public List<string> GetNotes()
        {
            return db.SelectAll("note", "notes");
        }

        
    }
}
