﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrainStorm
{
    public interface IConnection
    {
        void Connect();
        void Send(string action, byte[] message);
        void Exit();
    }
}
