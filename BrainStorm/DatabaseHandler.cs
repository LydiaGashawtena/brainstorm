﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static System.Console;

namespace BrainStorm
{
    public class DatabaseHandler
    {
        string connectionString = "Server=localhost;port=3306;Database=brainstorm;username=root;password=Lydia123;";
        public bool AddUser(string name)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {

                MySqlCommand command = new MySqlCommand($"insert into user (name, hasGroup, groupName) values('{name}', 'false', '')", mysqlCon);
                MySqlDataReader reader;
                try
                {
                    mysqlCon.Open();
                }
                catch (Exception ex)
                {
                    WriteLine(ex.Message);
                    return false;
                }
                reader = command.ExecuteReader();
                while (reader.Read())
                {                    
                }

            }
            return true;
        }

        public bool AddMember(string name)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {

                MySqlCommand command = new MySqlCommand($"insert into members (name, isConnected) values('{name}', 'true')", mysqlCon);
                MySqlDataReader reader;
                try
                {
                    mysqlCon.Open();
                }
                catch (Exception ex)
                {
                    WriteLine(ex.Message);
                    return false;
                }
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                }

            }
            return true;
        }

        public bool AddNote(string note)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {

                MySqlCommand command = new MySqlCommand($"insert into notes (note) values('{note}')", mysqlCon);
                MySqlDataReader reader;
                try
                {
                    mysqlCon.Open();
                }
                catch (Exception ex)
                {
                    WriteLine(ex.Message);
                    return false;
                }
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                }

            }
            return true;
        }

        public bool AddFile(string name, byte[] file)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {

                MySqlCommand command = new MySqlCommand($"insert into files (name, file) values('{name}', @file)", mysqlCon);
                command.Parameters.Add("@file", MySqlDbType.VarBinary).Value = file;
                MySqlDataReader reader;
                try
                {
                    mysqlCon.Open();
                }
                catch (Exception ex)
                {
                    WriteLine(ex.Message);
                    return false;
                }
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                }

            }
            return true;
        }

        public byte[] GetFile(string name)
        {
            byte[] toReturn = { };
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {                
                MySqlCommand command = new MySqlCommand($"select file from files where name = '{name}';", mysqlCon);
                MySqlDataReader reader;
                try
                {
                    mysqlCon.Open();
                }
                catch (Exception ex)
                {
                    WriteLine(ex.Message);
                }
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    toReturn = (byte[])reader["file"];
                }

            }
            return toReturn;
        }

        public string SelectOne(string col, string from, string where, string key)
        {
            string toReturn = "";
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                
                MySqlCommand command = new MySqlCommand($"select {col} from {from} where {where} = '{key}';", mysqlCon);
                MySqlDataReader reader;
                try
                {
                    mysqlCon.Open();
                }
                catch (Exception ex)
                {
                    WriteLine(ex.Message);
                }
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    toReturn = reader[col].ToString();
                }                

            }
            return toReturn;
        }

        public List<string> SelectAll(string col, string from)
        {
            List<string> toReturn = new List<string>();
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {

                MySqlCommand command = new MySqlCommand($"select * from {from};", mysqlCon);
                MySqlDataReader reader;
                try
                {
                    mysqlCon.Open();
                }
                catch (Exception ex)
                {
                    WriteLine(ex.Message);
                }
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    toReturn.Add(reader[col].ToString());
                }

            }
            return toReturn;
        }

        public bool Update(string table, string col, string value, string where, string key)
        {
            
            
            MySqlCommand command;
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                if (table == "files")
                {
                    command = new MySqlCommand($"update {table} set {col} = @file where {where} = '{key}'", mysqlCon);
                    command.Parameters.Add("@file", MySqlDbType.VarBinary).Value = Encoding.ASCII.GetBytes(value);
                }
                else
                {
                    command = new MySqlCommand($"update {table} set {col} = '{value}' where {where} = '{key}'", mysqlCon);
                }
                
                MySqlDataReader reader;
                try
                {
                    mysqlCon.Open();
                }
                catch (Exception ex)
                {
                    WriteLine(ex.Message);
                    return false;
                }
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                }

            }
            WriteLine("UPDATE" + col, value);
            return true;
        }

        public bool Update(string table, string col, byte[] value, string where, string key)
        {            
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                MySqlCommand command = new MySqlCommand($"update {table} set {col} = @file where {where} = '{key}'", mysqlCon);
                command.Parameters.Add("@file", MySqlDbType.VarBinary).Value = value;               
               
                MySqlDataReader reader;
                try
                {
                    mysqlCon.Open();
                }
                catch (Exception ex)
                {
                    WriteLine(ex.Message);
                    return false;
                }
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                }

            }
            WriteLine("UPDATE" + col + value);
            return true;
        }

        public Boolean CheckUser()
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {

                MySqlCommand command = new MySqlCommand("select * from user;", mysqlCon);
                MySqlDataReader reader;
                try
                {
                    mysqlCon.Open();
                }
                catch (Exception ex)
                {
                    WriteLine(ex.Message);
                }
                reader = command.ExecuteReader();
                int count = 0;
                while (reader.Read())
                {
                    count++;
                }
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        public bool Delete(string from)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {

                MySqlCommand command = new MySqlCommand($"truncate table {from}", mysqlCon);
                MySqlDataReader reader;
                try
                {
                    mysqlCon.Open();
                }
                catch (Exception ex)
                {
                    WriteLine(ex.Message);
                    return false;
                }
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                }

            }
            return true;
        }
    }
}
