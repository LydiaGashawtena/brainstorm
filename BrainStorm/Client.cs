﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static System.Console;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace BrainStorm
{
    public class Client : IConnection
    {
        private IPAddress ip;
        private Socket clientSocket;
        private const int PORT = 3355;
        private byte[] buffer;
        private Controller controller;

        public Client(Controller controller, string ip)
        {
            this.controller = controller;
            this.ip = IPAddress.Parse(ip);
        }
      
        private void ConnectCallback(IAsyncResult AR)
        {
            try
            {
                clientSocket.EndConnect(AR);
                buffer = new byte[2048];
                clientSocket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReceiveCallBack, clientSocket);
            }
            catch (SocketException e)
            {
                WriteLine(e.Message);
            }
            byte[] actionArr = Encoding.ASCII.GetBytes("NCON");
            byte[] message = Encoding.ASCII.GetBytes(controller.GetUser());
            //byte[] message = Encoding.ASCII.GetBytes("user2");
            List<byte> temp = new List<byte>();
            byte[] toSend = new byte[actionArr.Length + message.Length];
            for (int i = 0; i < actionArr.Length; i++)
            {
                temp.Add(actionArr[i]);
            }
            for (int i = 0; i < message.Length; i++)
            {
                temp.Add(message[i]);
            }

            toSend = temp.ToArray();
            clientSocket.BeginSend(toSend, 0, toSend.Length, SocketFlags.None, SendCallBack, null);
            WriteLine("Connected");
        }

        private void ReceiveCallBack(IAsyncResult AR)
        {
            try
            {
                int received = clientSocket.EndReceive(AR);

                if (received == 0)
                {
                    return;
                }

                byte[] recBuf = new byte[received];
                Array.Copy(buffer, recBuf, received);
                string text = Encoding.ASCII.GetString(recBuf);
                //Console.WriteLine("Received Text: " + text);

                byte[] action = new byte[4];
                byte[] main = new byte[received - 4];
                Array.Copy(recBuf, 0, action, 0, 4);
                Array.Copy(recBuf, 4, main, 0, received - 4);
                
                string actionString = Encoding.ASCII.GetString(action);
                string mainString = Encoding.ASCII.GetString(main);
                WriteLine("In client received action: " + actionString);
                WriteLine("In client received main: " + mainString);
                switch (actionString)
                {
                    case "DRAW":
                        controller.Draw(main);
                        break;
                    case "MEML":
                        controller.setMembers(mainString);
                        Send("GNOT", new byte[0]);
                        break;
                    case "NOTL":
                        List<string> notes = mainString.Split(',').ToList();
                        controller.SetNotesList("CLIENT", notes);
                        break;
                    case "NMEM":
                        WriteLine(actionString + ", " + mainString);
                        controller.AddNewMember(mainString, "CLIENT");
                        break;
                    case "GFLS":
                        List<string> files = mainString.Split(',').ToList();
                        controller.SetFilesForClient(files);
                        break;
                    case "GFIL":
                        controller.SetAFileForClient(main);
                        break;
                    case "NNOT":
                        controller.UpdateNotesList(mainString);
                        break;
                    case "EXIT":
                        controller.Exit();
                        break;
                }
               
                clientSocket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReceiveCallBack, clientSocket);
            }
          
            catch (SocketException ex)
            {
                WriteLine(ex.Message);                
            }
            catch (ObjectDisposedException ex)
            {
                WriteLine(ex.Message);                
            }
        }

        private void SendCallBack(IAsyncResult AR)
        {
            try
            {
                clientSocket.EndSend(AR);
            }
            catch (SocketException ex)
            {
                WriteLine(ex.Message);
            }
            catch (ObjectDisposedException ex)
            {
                WriteLine(ex.Message);
            }
           
        }       
        public void Exit()
        {
            Send("EXIT", Encoding.ASCII.GetBytes(controller.GetUser())); 
            clientSocket.Shutdown(SocketShutdown.Both);
            clientSocket.Close();
            //Environment.Exit(0);
        }

        public void Connect()
        {
            try
            {
                clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                var endPoint = new IPEndPoint(ip, PORT);
                clientSocket.BeginConnect(endPoint, ConnectCallback, null);
            }
            catch (SocketException ex)
            {
                WriteLine(ex.Message);
            }
            catch (ObjectDisposedException ex)
            {
                WriteLine(ex.Message);
            }
        }

        public void Send(string action, byte[] message)
        {                   
                byte[] actionArr = Encoding.ASCII.GetBytes(action);
                WriteLine("Action Length: " + actionArr.Length);
                List<byte> temp = new List<byte>();
                byte[] toSend = new byte[actionArr.Length + message.Length];
                for (int i = 0; i < actionArr.Length; i++)
                {
                    temp.Add(actionArr[i]);
                }
                for (int i = 0; i < message.Length; i++)
                {
                    temp.Add(message[i]);
                }

                toSend = temp.ToArray();
                clientSocket.BeginSend(toSend, 0, toSend.Length, SocketFlags.None, SendCallBack, null);
            //Console.Write("Sent: " + msg);
        }

    }
}
