﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static System.Console;

namespace BrainStorm
{
    /// <summary>
    /// Interaction logic for StartWindow.xaml
    /// </summary>
    public partial class StartWindow : Window
    {
        DatabaseHandler db = new DatabaseHandler();
        MainWindow mainWindow = new MainWindow();
        public StartWindow()
        {
            if (db.CheckUser())
            {
                mainWindow.Show();
                this.Close();
            }
            else
            {
                WindowStartupLocation = WindowStartupLocation.CenterScreen;
                InitializeComponent();
            }
            
        }

        private void btn_toMain(object sender, RoutedEventArgs e)
        {           
            string name = username.Text;
            db.AddUser(name);
            //WriteLine("User: " + db.Select());           
            mainWindow.Show();
            this.Close();
        }
    }
}
