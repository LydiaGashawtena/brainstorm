﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Ink;
using Microsoft.VisualBasic;
using System.IO;
using System.Windows.Media.Effects;
using System.ComponentModel;

namespace BrainStorm
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Controller controller;
        public Boolean hasGroup = false;
        public string group;
        public List<string> members;
        public List<string> notes;
        public string username;
        private string userStatus;
        private StackPanel membersPanel = new StackPanel();
        private StackPanel notesPanel = new StackPanel();
        private Button disconnect = new Button();


        public MainWindow()
        {
            this.controller = new Controller(this);
            this.username = controller.GetUser();
            InitializeComponent();
            AddHandler(FrameworkElement.MouseDownEvent, new MouseButtonEventHandler(mouseDown_event), true);
            setInitialStates();
        }

        private void setInitialStates()
        {
            group = controller.CheckGroup();
            if (group != "")
            {
                hasGroup = true;
                start.Visibility = Visibility.Visible;              
            }
            else
            {
                return;
            }
        }

        public void MakeMembersList(string _for, List<string> membersList = null)
        {
            if (_for == "SERVER")
            {
                members = controller.GetMembersForServer();
            }
            else
            {
                members = membersList;
            }
            // title.Content = "Members";
            this.Dispatcher.Invoke((Action)delegate
            {
                create.Visibility = Visibility.Collapsed;
                join.Visibility = Visibility.Collapsed;
                start.Visibility = Visibility.Collapsed;
                membersPanel.Visibility = Visibility.Collapsed;
                disconnect.Visibility = Visibility.Collapsed;

                membersPanel = new StackPanel();
                Label membersLabel = new Label();
                membersLabel.FontSize = 30;
                membersLabel.HorizontalAlignment = HorizontalAlignment.Center;
                membersLabel.Content = "Members";
                Thickness membersMargin = membersLabel.Margin;
                membersMargin.Top = 10;
                membersLabel.Margin = membersMargin;
                membersLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFFFF"));
                membersLabel.Effect = new DropShadowEffect{ShadowDepth = 1, Direction = 2, Color = new Color { A = 0, R = 221, G = 221, B = 221 }, Opacity = 0.5, BlurRadius = 4};
                membersPanel.Children.Add(membersLabel);
                for (int i = 0; i < members.Count; i++)
                {
                    StackPanel sp = new StackPanel();
                    sp.Orientation = Orientation.Horizontal;
                    Label nameLabel = new Label();
                    nameLabel.Width = 150;
                    nameLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFFFF"));
                    nameLabel.FontSize = 22;
                    Thickness margin = nameLabel.Margin;
                    margin.Left = 15;
                    margin.Top = 10;
                    nameLabel.Margin = margin;
                    nameLabel.Content = members[i];
                    Label conLabel = new Label();
                    Thickness conMargin = conLabel.Margin;
                    conMargin.Top = 18;
                    conLabel.Margin = conMargin;
                    conLabel.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF93C9FF"));
                    if(_for == "SERVER" && controller.isConnected(members[i], _for) == true)
                    {
                        conLabel.Content = "connected";
                    }else if(_for == "CLIENT" && controller.isConnected(members[i], _for, i))
                    {
                        conLabel.Content = "connected";
                    }
                    else
                    {
                        conLabel.Content = "";
                    }
                    sp.Children.Add(nameLabel);
                    sp.Children.Add(conLabel);
                    membersPanel.Children.Add(sp);
                
                }

                disconnect = new Button();
                TextBlock t = new TextBlock();
                t.Text = "Disconnect";
                t.FontSize = 22;
                t.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFD9D9D9"));
                //t.Effect = new DropShadowEffect { ShadowDepth = 1, Direction = 2, Color = new Color { A = 0, R = 221, G = 221, B = 221 }, Opacity = 0.5, BlurRadius = 4 };
                disconnect.Content = t;
                disconnect.Style = (Style)Application.Current.Resources["AsideButton"];
                disconnect.Margin = (Thickness)Application.Current.Resources["ButtonMargins"];
                disconnect.Click += btn_disconnect;
                aside.Children.Add(membersPanel);
                aside.Children.Add(disconnect);
                
            });
        }

        public void MakeNotesList(List<string> notes)
        {
            WriteLine("Make notes list: " + userStatus);
            this.Dispatcher.Invoke((Action)delegate
            {
                notesPanel.Visibility = Visibility.Collapsed;
            this.notes = notes;
            notesPanel = new StackPanel();
            foreach (string note in notes)
            {
                Button button = new Button();
                button.Style = (Style)Application.Current.Resources["NoteButton"];
                TextBlock textBlock = new TextBlock();
                textBlock.Style = (Style)Application.Current.Resources["NoteText"];
                textBlock.Text = note;
                button.Content = textBlock;
                notesPanel.Children.Add(button);
            }
            rightPanel.Children.Add(notesPanel);
            });
        }
        
            
        public void Draw(StrokeCollection collection)
        {
            this.Dispatcher.Invoke((Action)delegate
            {
                canvas.Strokes = collection;
            });
            
        }

        private void btn_create(object sender, RoutedEventArgs e)
        {
            userStatus = "SERVER";
            controller.CreateNewGroup();
            controller.Connect();
            if (hasGroup)
            {
                controller.RemoveGroup();
            }
            MakeMembersList("SERVER");
        }

        private void btn_start(object sender, RoutedEventArgs e)
        {
            userStatus = "SERVER";
            controller.Connect();
            MakeMembersList("SERVER");
            MakeNotesList(controller.GetNotes());
        }

        private void btn_join(object sender, RoutedEventArgs e)
        {
            userStatus = "CLIENT";
            InputWindow inputWindow = new InputWindow("IP");
            inputWindow.ShowDialog();
            string ip = inputWindow.inputText;
            if (ip == null)
            {
                return;
            }           
            WriteLine(ip);
            controller.Connect(ip);            
        }

        private void btn_disconnect(object sender, RoutedEventArgs e)
        {
            if (userStatus == "SERVER")
            {
                controller.Send("EXIT", "");
            }
            
            controller.Exit();
        }

        public void SetDisconnectedState()
        {
            this.Dispatcher.Invoke((Action)delegate
            {
                membersPanel.Visibility = Visibility.Collapsed;
                disconnect.Visibility = Visibility.Collapsed;
                create.Visibility = Visibility.Visible;
                join.Visibility = Visibility.Visible;
                if (hasGroup == true)
                {
                    start.Visibility = Visibility.Visible;
                }
                notesPanel.Visibility = Visibility.Collapsed;
            });
            
        }

        #region Inkcanvas Events
        private void btn_send(object sender, RoutedEventArgs e)
        {
            StrokeCollection collection = canvas.Strokes;
            if (controller.Send(collection) == false)
            {
                WriteLine("Can't Send");
            }
            
        }

        private void key_send(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                StrokeCollection collection = canvas.Strokes;
                if (controller.Send(collection) == false)
                {
                    InputWindow inputWindow = new InputWindow("MESSAGE");
                    inputWindow.ShowDialog();
                    WriteLine("Can't Send");
                }

            }
        }

       
        private void btn_pen(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 50; i++)
            {
                canvas.MouseDown -= mouseDown_event;
                canvas.MouseUp -= mouseUp_event;
                canvas.MouseMove -= mouseMove_event;
            }
            
            canvas.EditingMode = InkCanvasEditingMode.Ink;
            
        }

        Point startPoint = new Point(15.0, 15.0), end;
        String shape;

        public void btn_line(object sender, RoutedEventArgs e)
        {
            canvas.EditingMode = InkCanvasEditingMode.None;
            canvas.MouseDown += mouseDown_event;
            canvas.MouseUp += mouseUp_event;
            canvas.MouseMove += mouseMove_event;
            shape = "LINE";
        }

        public void btn_rectangle(object sender, RoutedEventArgs e)
        {
            canvas.EditingMode = InkCanvasEditingMode.None;
            canvas.MouseDown += mouseDown_event;
            canvas.MouseUp += mouseUp_event;
            canvas.MouseMove += mouseMove_event;
            shape = "RECTANGLE";
        }
        private void mouseDown_event(object sender, MouseButtonEventArgs e)
        {
            WriteLine("Mouse Down");
            startPoint = e.GetPosition(canvas);
            WriteLine("Down: " + startPoint.X + ", " + startPoint.Y);
        }

        Stroke st;
        private void mouseUp_event(object sender, MouseButtonEventArgs e)
        {
            StylusPointCollection pts;
            switch (shape)
            {
                case "LINE":
                    pts = new StylusPointCollection();
                    pts.Add(new StylusPoint(end.X, end.Y));
                    pts.Add(new StylusPoint(startPoint.X, startPoint.Y));
                    st = new Stroke(pts);
                    break;
                case "RECTANGLE":
                    pts = new StylusPointCollection();
                    pts.Add(new StylusPoint(end.X, end.Y));
                    pts.Add(new StylusPoint(end.X, startPoint.Y));
                    pts.Add(new StylusPoint(startPoint.X, startPoint.Y));
                    pts.Add(new StylusPoint(startPoint.X, end.Y));
                    pts.Add(new StylusPoint(end.X, end.Y));
                    st = new Stroke(pts);
                    break;
            }
            st.DrawingAttributes.Color = attr.Color;
            st.DrawingAttributes.Width = attr.Width;
            st.DrawingAttributes.Height = attr.Height;
            canvas.Strokes.Add(st);
        }

        private void mouseMove_event(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                end = e.GetPosition(canvas);
                //WriteLine("Move: " + end.X + ", " + end.Y);
            }
        }

        private void btn_erase(object sender, RoutedEventArgs e)
        {
           
            for (int i = 0; i < 50; i++)
            {
                canvas.MouseDown -= mouseDown_event;
                canvas.MouseUp -= mouseUp_event;
                canvas.MouseMove -= mouseMove_event;
            }
            canvas.EditingMode = InkCanvasEditingMode.EraseByStroke;
        }

        private void btn_color(object sender, RoutedEventArgs e)
        {
            var tag = ((Button)sender).Tag;
            switch (tag)
            {
                case "Red":
                    attr.Color = (Color)ColorConverter.ConvertFromString("Red");
                    break;
                case "Yellow":
                    attr.Color = (Color)ColorConverter.ConvertFromString("Yellow");
                    break;
                case "Green":
                    attr.Color = (Color)ColorConverter.ConvertFromString("Green");
                    break;
                case "Blue":
                    attr.Color = (Color)ColorConverter.ConvertFromString("Blue");
                    break;
                case "Black":
                    attr.Color = (Color)ColorConverter.ConvertFromString("Black");
                    break;
                case "Gray":
                    attr.Color = (Color)ColorConverter.ConvertFromString("Gray");
                    break;
            }
        }

        private void btn_size(object sender, RoutedEventArgs e)
        {
            var tag = ((Button)sender).Tag;
            int size = Int32.Parse(penSize.Content.ToString());          
            
            switch (tag)
            {
                case "up":
                    attr.Width++;
                    attr.Height++;
                    penSize.Content = size + 1;
                    break;
                case "down":
                    if (size <= 1)
                    {
                        size = 1;
                        attr.Width = 1;
                        attr.Height = 1;
                    }
                    else
                    {
                        attr.Width--;
                        attr.Height--;
                        penSize.Content = size - 1;                            
                    }
                    break;

                }            
            
        }

        private void btn_clear(object sender, RoutedEventArgs e)
        {
            canvas.Strokes.Clear();
        }

        private void btn_save(object sender, RoutedEventArgs e)
        {
            InputWindow inputWindow = new InputWindow("FILENAME");
            inputWindow.ShowDialog();
            string fileName = inputWindow.inputText;
            if (fileName == null)
            {
                return;
            }
            byte[] file;
            using (MemoryStream ms = new MemoryStream())
            {
                canvas.Strokes.Save(ms);
                file = ms.ToArray();
                
            }
            controller.Save(userStatus, fileName, file);
        }

        private void btn_open(object sender, RoutedEventArgs e)
        {
            string fileName;
            ChooseFileWindow fileWindow = new ChooseFileWindow(controller, "SERVER");
            if (userStatus == "CLIENT")
            {
                controller.GetFiles(fileWindow, "CLIENT");
            }
            else
            {
                fileWindow.SetList(controller.GetFiles(fileWindow, "SERVER"));
            }
                        
            fileWindow.ShowDialog();
            fileName = fileWindow.selected;
            WriteLine("FileName:" + fileName + ";");
            if (fileName == null)
            {
                WriteLine("FileName:" + fileName + ";");
                return;
            }
            WriteLine("In main: " + fileName);

            controller.Open(fileName, userStatus);
        }

        #endregion

        public void btn_addNote(object sender, RoutedEventArgs e)
        {
            InputWindow inputWindow = new InputWindow("NOTE");
            inputWindow.ShowDialog();
            string note = inputWindow.inputText;
            if (note == null)
            {
                return;
            }
            controller.AddNote(userStatus, note);          
            
        }

        private void Closing_event(object sender, CancelEventArgs e)
        {
            if(userStatus == "SERVER")
            {
                try
                {
                    for (int i = 0; i < members.Count; i++)
                    {
                        controller.setConnected(members[i], false);
                    }

                    WriteLine("CLOSE");
                    if (controller.isConnected())
                    {
                        controller.Exit();
                    }
                }
                catch (Exception ex)
                {
                    WriteLine("In closing: " + ex.Message);
                }
            }
            
            
            

          
        }
    }    
       
}

