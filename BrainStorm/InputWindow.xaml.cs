﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BrainStorm
{
    /// <summary>
    /// Interaction logic for InputWindow.xaml
    /// </summary>
    public partial class InputWindow : Window
    {
        public string _for;
        public string inputText;
        public InputWindow(string _for)
        {
            this._for = _for;
            InitializeComponent();
            if (_for == "IP")
            {
                promptLabel.Content = "Please Enter the Ip Address of the Host Computer";
                input.Text = "127.0.0.1";
            }
            else if(_for == "FILENAME")
            {
                promptLabel.Content = "Please Enter a Name for your File";
            }
            else if (_for == "NOTE")
            {
                Thickness labelMargin = promptLabel.Margin;
                labelMargin.Left = 30;
                labelMargin.Top = 10;
                promptLabel.Margin = labelMargin;
                promptLabel.Content = "Add Note";

                Thickness inputMargin = input.Margin;
                inputMargin.Left = 30;
                inputMargin.Top = 50;
                input.Margin = inputMargin;
                input.Height = 60;

                Thickness okMargin = ok.Margin;
                Thickness cancelMargin = cancel.Margin;
                okMargin.Top = 125;
                cancelMargin.Top = 125;
                okMargin.Left = 115;
                cancelMargin.Left = 225;
                ok.Margin = okMargin;
                cancel.Margin = cancelMargin;
            }else if(_for == "MESSAGE")
            {
                this.Width = 400;
                this.Height = 150;

                Thickness labelMargin = promptLabel.Margin;
                labelMargin.Left = 30;
                labelMargin.Top = 25;
                promptLabel.Margin = labelMargin;

                Thickness buttonMargin = cancel.Margin;
                buttonMargin.Left = 250;
                buttonMargin.Top = 75;
                cancel.Margin = buttonMargin;
                cancel.Content = "Ok";

                promptLabel.Content = "Please Connect to a Group to Send.";
                input.Visibility = Visibility.Collapsed;
                ok.Visibility = Visibility.Collapsed;
            }
            
        }

        private void btn_ok(object sender, RoutedEventArgs e)
        {
            inputText = input.Text;
            this.Close();
        }

        private void btn_cancel(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
